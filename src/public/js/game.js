
var starsList = []
var asteroidList = []
var aster; 
var startSpawning = false
var ship;
var startTimer;
var updateTimer;
var endTimer;
var started = false;

var bestSeconds = 0;
var bestMinutes = 0;
var bestRemainingMiliseconds = 0



function startGame() {
    // stvori zvijezde
    for(let i = 0; i < 100; i++) {
        var radius = getRandomFloat(0.5, 3)
        var color = '#56FFEE' 
        var x = getRandomFloat(5, window.innerWidth)
        var y = getRandomFloat(-window.innerHeight/1.5, -window.innerHeight/4.)
        var speed_y = getRandomFloat(0.5, 1.5)
        var star = new stars(radius, color, x, y, speed_y)
        starsList[i] = star;
    }
    //stvori igrača koji predstavlja ship na sredini ekrana
    ship = new player(40,40,2,2,window.innerWidth/2 - 20, window.innerHeight/2 - 20, "red")
    // provjeri postoji li najbolji rezultat u localStorage-u i preuzmi ga
    if(typeof(Storage) !== undefined) {
        if(localStorage.getItem('bestResult')) {
            var res = localStorage.getItem('bestResult')
            bestSeconds = Math.floor(res / 1000);
            bestMinutes = Math.floor(bestSeconds / 60);
            bestSeconds = bestSeconds%60
            bestRemainingMiliseconds = res % 1000;
        } 
    }
    gameArea.start()
}

var gameArea = {
    canvas: document.createElement("canvas"),
    start: function() {
        this.canvas.id = "canvas",
        this.borderSize = 5,
        this.canvas.width = window.innerWidth - this.borderSize;
        this.canvas.height =  window.innerHeight - this.borderSize;
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0])
        this.frameNo=0;
        // pozivanje gameLoopa
        this.interval = setInterval(updateGameArea, 15)
        // iscrtavanje pozadine kao gradijent prvi put
        this.grd = this.context.createLinearGradient(this.borderSize, this.borderSize, this.borderSize, this.canvas.height)
        this.grd.addColorStop(0, '#1b181f')
        this.grd.addColorStop(1, '#320459')
        this.canvasWindow()
    },
    stop: function() {
        clearInterval(this.interval)
    },
    clear: function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
        this.canvasWindow()
    },

    restart: function() {
        clearInterval(this.interval)
        // vrati ship na sredinu prozora i izbirši sve asteroide
        ship = new player(40,40,2,2,window.innerWidth/2 - 20, window.innerHeight/2 - 20, "red")
        asteroidList = []
        // izračunaj vrijeme trenutne igre
        var milliseconds = endTimer - startTimer;
        var seconds = Math.floor(milliseconds / 1000);
        var minutes = Math.floor(seconds / 60);
        seconds = seconds % 60;
        var remainingMilliseconds = milliseconds % 1000;
        // provjeri koje je veće vrijeme u localstorage-u i spremi veće
        if(typeof(Storage) !== undefined) {
            if(localStorage.getItem('bestResult')) {
                var res = localStorage.getItem('bestResult')
                if(res < milliseconds) {
                    localStorage.setItem('bestResult', milliseconds)
                }
            } else {
                localStorage.setItem('bestResult', milliseconds)
            }
        } else {
            console.log("HTML5 Web Storage funkcionalnost nije podržana")
        }
        // pokreni vrijeme ponovo, uzmi vrijeme iz localstorage-a i započni novi game loop 
        startTimer = new Date()
        if(typeof(Storage) !== undefined) {
            if(localStorage.getItem('bestResult')) {
                var res = localStorage.getItem('bestResult')
                bestSeconds = Math.floor(res / 1000);
                bestMinutes = Math.floor(bestSeconds / 60);
                bestSeconds = bestSeconds%60
                bestRemainingMiliseconds = res % 1000;
            } 
        }
        this.interval = setInterval(updateGameArea, 15)

    },
    // iscrtaj obrub i gradijent za pozadinu
    canvasWindow: function(){
        // obrub
        this.context.lineWidth = this.borderSize;
        this.context.strokeStyle = "#E95900"
        this.context.strokeRect(0, 0, this.canvas.width, this.canvas.height)
    
        // pozadina
        this.context.fillStyle = this.grd;
        this.context.fillRect(this.borderSize, this.borderSize, this.canvas.width, this.canvas.height)
    }
}


function updateGameArea(){
    gameArea.clear();
    // iscrtavaj zvijezde i ažuriraj novu poziciju
    for(let i = 0; i < starsList.length; i++) {
        starsList[i].newPos();
        starsList[i].update();
    }
    // spawnaj asteroide nakon sto je prva zvijezda pala do dna
    if(asteroidList.length <= 10 && startSpawning) {
        for (let i = 0; i < 15; i++) {
            var aster = spawnAsteroid()
            asteroidList.push(aster)
        }
    }
    // ažuriraj poziciju asteroida
    for(let i = 0; i < asteroidList.length; i++) {
        asteroidList[i].newPos(i);
        if(asteroidList[i].destroy){
            asteroidList.splice(i, 1);
            continue;
        }
        
    }
    // izračunaj novu poziciju shipa i provjeri ima li kolozije s asteroidima
    ship.newPos()
    ship.checkCollision()

    // iscrtaj asteroide i ship
    for(let i = 0; i < asteroidList.length; i++) {
        asteroidList[i].update();
    }
    ship.update()
    
    // ažuriranje vremena
    updateTimer = new Date()
    var milliseconds = updateTimer - startTimer;
    var seconds = Math.floor(milliseconds / 1000);
    var minutes = Math.floor(seconds / 60);
    var remainingMilliseconds = milliseconds % 1000
    seconds = seconds % 60;
    if(minutes != 0 && !minutes) {
        minutes = 0;
        seconds = 0;
        remainingMilliseconds = 0;
    }
    // iscrtavanje vremena
    gameArea.context.save()
    gameArea.context.font = "20px Georgia"
    gameArea.context.fillStyle = "white"
    gameArea.context.fillText(`Best: ${bestMinutes} : ${bestSeconds} : ${bestRemainingMiliseconds}`, gameArea.canvas.width -180, 30)
    gameArea.context.fillText(`${minutes} : ${seconds} : ${remainingMilliseconds}`, gameArea.canvas.width -120, 60)
    gameArea.context.restore();

    
}

//player
class player {
    constructor(width, height, speed_x, speed_y, x, y, color) {
        this.width = width;
        this.height = height;
        this.speed_x = speed_x;
        this.speed_y = speed_y;
        this.move_x = 0;
        this.move_y = 0;
        this.color = color;
        this.x = x;
        this.y = y;
        this.isMovingLeft = false;
        this.isMovingRight = false;
        this.isMovingUp = false;
        this.isMovingDown = false;
        this.update = function () {
            var context = gameArea.context;
            context.save();
            context.translate(this.x, this.y);
            context.shadowBlur = 20;
            context.shadowColor = "#FF7A7A";
            context.fillStyle = this.color;
            context.fillRect(0, 0, this.width, this.height);
            context.restore();
        };
        // određivanje kretanja na osnovi jesu li varijable aktivirane preko event listenera za strelice na tipkovnici
        this.newPos = function() {
            if (this.isMovingLeft) this.move_x = -this.speed_x;
            if (this.isMovingRight) this.move_x = this.speed_x;
            if (this.isMovingUp) this.move_y = -this.speed_y;
            if (this.isMovingDown) this.move_y = this.speed_y;
        

            if(!this.isMovingLeft && !this.isMovingRight) {
                this.move_x = 0;
            }

            if(!this.isMovingUp && !this.isMovingDown) {
                this.move_y = 0;
            }
            // ship ne smije izaći van canvasa
            if (this.x < 5) this.x = 5;
            if (this.x + this.width > canvas.width) this.x = gameArea.canvas.width - this.width;
            if (this.y < 5) this.y = 5;
            if (this.y + this.height > canvas.height) this.y = gameArea.canvas.height - this.height;
            
            this.x += this.move_x;
            this.y += this.move_y
        }
        // provjera kolizije shipa i broda
        this.checkCollision = function(){
            for(let i = 0; i < asteroidList.length; i++) {
                let aster = asteroidList[i];
                if(this.x < aster.x + aster.width &&  this.x + this.width > aster.x &&
                    this.y < aster.y + aster.height && this.y + this.height > aster.y) {
                        // zapamti vrijeme kraja igre i resetiraj igru
                        endTimer = new Date()
                        gameArea.restart() 
                    }
            }
        }
    }
}


// stars
class stars {
    constructor(radius, color, x, y, speed_y) {
        this.radius = radius;
        this.speed_y = speed_y;
        this.color = color;
        this.x = x;
        this.y = y;
        //iscrtavanje zvijezda (krug sa sjenom)
        this.update = function () {
            var context = gameArea.context;
            context.save();
            //context.translate(this.x, this.y);
            context.shadowBlur = 10;
            context.shadowColor = "#FFFFFF";
            context.beginPath();
            context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
            context.fillStyle = this.color;
            context.fill();
            context.closePath();
            context.restore();
        };
        // zvijezde padaju po y osi i vraćaju se na -10piksela iznad y osi
        this.newPos = function () {
            if (this.y > window.innerHeight + 10) {
                this.y = -10;
                this.speed_y = getRandomFloat(0.5, 1.5);
                this.radius = getRandomFloat(0.5, 3);
                startSpawning = true;
                // započni timer na početku
                if(!started){
                    startTimer = new Date()
                    started = true;
                }
                
            } else {
                this.y += this.speed_y;
            }
        };
    }
}

// asteroids
class asteroid {
    constructor(width, height, speed_x, speed_y, x, y, color) {
        this.width = width;
        this.height = height;
        this.speed_x = speed_x;
        this.speed_y = speed_y;
        this.color = color;
        this.x = x;
        this.y = y;
        this.destroy = false;
        // iscrtavanje kvadrata sa sjenom
        this.update = function () {
            var context = gameArea.context;
            context.save();
            context.translate(this.x, this.y);
            context.shadowBlur = 20;
            context.shadowColor = "gray";
            context.fillStyle = this.color;
            context.fillRect(0, 0, this.width, this.height);
            context.restore();
        };
        // ažuriranje pozicije i provjera kada se asteroidi moraju uništiti (nakon što izaću iz određene granice)
        this.newPos = function (index) {
            this.x += this.speed_x;
            this.y += this.speed_y;

            if ((this.x < -200 || this.x > window.innerWidth + 200) ||
                this.y < -200 || this.y > window.innerHeight + 200) {
                this.destroy = true;
            } else {
                this.destroy = false;
            }

        };
    }
}

// određivanje gdje se asteroid spawna i prilagođavanje brzine da ode na suprotnu stranu
function spawnAsteroid() {
    var width;
    var height;
    var speed_x;
    var speed_y;
    var x;
    var y;
    let spawnPos = getRandomInt(1, 4);
    // primjer ako se asteroid stvori na top-u
    if(spawnPos == 1) {
        width = getRandomFloat(20, 80)
        height = width
        speed_x;
        if(getRandomInt(1,2) == 2) {
            speed_x = getRandomFloat(0, 1.5)
        } else {
            speed_x = getRandomFloat(-1.5, 0)
        }
        speed_y = getRandomFloat(1.5, 4)
        x = getRandomFloat(10, window.innerWidth - 10)
        y = getRandomFloat(-180, -100)
        // primjer ako se asteroid stvori right
    } else if (spawnPos == 2) {
        width = getRandomFloat(20, 80)
        height = width
        speed_x = getRandomFloat(-1.5, -4)
        speed_y;
        if(getRandomInt(1,2) == 2) {
            speed_y = getRandomFloat(0, 1.5)
        } else {
            speed_y = getRandomFloat(-1.5, 0)
        }
        x = getRandomFloat(window.innerWidth + 100, window.innerWidth+180)
        y = getRandomFloat(10, window.innerHeight-10)
        // primjer ako se asteroid stvori bottom
    } else if (spawnPos == 3) {
        width = getRandomFloat(20, 80)
        height = width
        speed_x;
        if(getRandomInt(1,2) == 2) {
            speed_x = getRandomFloat(0, 1.5)
        } else {
            speed_x = getRandomFloat(-1.5, 0)
        }
        speed_y = getRandomFloat(-4, -1.5)
        x = getRandomFloat(10, window.innerWidth - 10)
        y = getRandomFloat(window.innerHeight + 100, window.innerHeight+180)
        // primjer ako se asteroid stvori left
    } else {
        width = getRandomFloat(20, 80)
        height = width
        speed_x = getRandomFloat(0, 1.5)
        speed_y;
        if(getRandomInt(1,2) == 2) {
            speed_y = getRandomFloat(0, 1.5)
        } else {
            speed_y = getRandomFloat(-1.5, 0)
        }
        x = getRandomFloat(-180, -100)
        y = getRandomFloat(10, window.innerHeight)
    }
    var aster = new asteroid(width, height,speed_x, speed_y, x, y,getRandomGray())
    return aster
}

// određivanje random sive boje
function getRandomGray() {
    var val = Math.floor(100 + Math.random() * 100);
    var gray = 'rgb(' + val + ',' + val + ',' + val + ')';
    return gray;
}

// određivanje random decimalnog broja
function getRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
}

// određivanje random cijelog broja
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// ako je strelica pritisnuta, dopusti ship-u da se kreće
function handlePlayerKeyDown(event) {
    if (event.code === 'ArrowLeft') {
        ship.isMovingLeft = true;
    } else if (event.code === 'ArrowRight') {
        ship.isMovingRight = true;
    } else if (event.code === 'ArrowUp') {
        ship.isMovingUp = true;
    } else if (event.code === 'ArrowDown') {
        ship.isMovingDown = true;
    }
}

// ako je strelica pritisnuta, ne dopusti ship-u da se kreće
function handlePlayerKeyUp(event) {
    if (event.code === 'ArrowLeft') {
        ship.isMovingLeft = false;
    } else if (event.code === 'ArrowRight') {
        ship.isMovingRight = false;
    } else if (event.code === 'ArrowUp') {
        ship.isMovingUp = false;
    } else if (event.code === 'ArrowDown') {
        ship.isMovingDown = false;
    }
}

window.onload = startGame()
// event listeneri za strelice na tipkovnici
window.addEventListener('keydown', handlePlayerKeyDown)
window.addEventListener('keyup', handlePlayerKeyUp)

